﻿using Newtonsoft.Json;
using System.IO;
using System.ServiceProcess;

namespace PapaScan
{
    public partial class PapaScan : ServiceBase
    {
        private HttpServer http;
        private Scanner.Service scanner = new Scanner.Service();
        private Uploader uploader = new Uploader();
        public PapaScan()
        {
            http = new HttpServer(10001);
            scanner.Scanned += (_, e) =>
            {
                uploader.Add(e);
            };
            http.HandlerFunc("scan", (request, response) => {
                var scanRequest = Deserialize<Scanner.Request>(request.InputStream);
                if (scanRequest != null)
                {
                    scanner.AddJob(scanRequest);
                }
                response.StatusCode = 200;
            });
            http.HandlerFunc("properties", (request, response) => {
                Serialize(response.OutputStream, scanner.GetProperties());
            });
            http.HandlerFunc("state", (request, response) => {
                Serialize(response.OutputStream, new { busy = scanner.Busy });
            });
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            http.Start();
            scanner.Start();
            uploader.Start();
        }

        protected override void OnStop()
        {
            http.Stop();
            scanner.Stop();
            uploader.Stop();
        }

        private static void Serialize(Stream s, object value)
        {
            using (StreamWriter writer = new StreamWriter(s))
            using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
            {
                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(jsonWriter, value);
                jsonWriter.Flush();
            }
        }

        private static T Deserialize<T>(Stream s)
        {
            using (StreamReader reader = new StreamReader(s))
            using (JsonTextReader jsonReader = new JsonTextReader(reader))
            {
                JsonSerializer ser = new JsonSerializer();
                return ser.Deserialize<T>(jsonReader);
            }
        }
    }
}
