﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapaScan.Scanner
{
    class ScanResultEventArgs : EventArgs
    {
        public string Page { get; set; }
        public int Number { get; set; }
        public Request Request { get; set; }
        public string Error { get; set; }
        public string Trace { get; set; }

        public ScanResultEventArgs Clone()
        {
            return new ScanResultEventArgs
            {
                Page = this.Page,
                Number = this.Number,
                Request = this.Request,
                Error = this.Error,
                Trace = this.Trace,
            };
        }
    }
}
