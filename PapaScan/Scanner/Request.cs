﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapaScan.Scanner
{
    class Request
    {
        [JsonProperty("callback")]
        public string Callback { get; set; }
        [JsonProperty("scanner_id")]
        public string ScannerId { get; set; }
        [JsonProperty("resolution")]
        public int Resolution { get; set; }
    }
}
