﻿using MessagingToolkit.Barcode;
using NTwain;
using NTwain.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Test.Scanner
{
    class Service
    {
        private ConcurrentQueue<Request> queue = new ConcurrentQueue<Request>();
        private bool running = true;
        private readonly object runningLock = new object();
        public event EventHandler<ScanResultEventArgs> Scanned;
        public List<Properties> Properties { get; }
        public Service()
        {
            this.Properties = new List<Properties>();
            var twain = new TwainSession(TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly()));
            twain.Open();
            foreach (var s in twain)
            {
                Properties.Add(new Properties
                {
                    Id = s.Id,
                    Name = s.Name
                });
            }
            twain.Close();
        }

        public void Stop()
        {
            lock (runningLock)
                this.running = false;
        }

        public void Start()
        {
            lock (runningLock)
                this.running = true;
            ThreadPool.QueueUserWorkItem((e) =>
            {
                this.worker();
            });
        }

        public void AddJob(Request request)
        {
            this.queue.Enqueue(request);
        }

        private void worker()
        {
            var tmpPath = Path.Combine(Path.GetTempPath(), "PapaScan");
            if (!Directory.Exists(tmpPath))
            {
                Directory.CreateDirectory(tmpPath);
            }
            while (true)
            {
                lock (runningLock)
                    if (!this.running)
                        break;
                if (this.queue.TryDequeue(out Request request))
                {
                    var twain = new TwainSession(TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly()));
                    int counter = 0;
                    twain.DataTransferred += (object sender, DataTransferredEventArgs e) =>
                    {
                        counter++;
                        if (e.NativeData != IntPtr.Zero)
                        {
                            var filename = Path.Combine(tmpPath, string.Format("{0}.jpg", counter));
                            Image.FromStream(e.GetNativeImageStream()).Save(filename, ImageFormat.Jpeg);

                            this.Scanned?.Invoke(null, new ScanResultEventArgs
                            {
                                Page = filename,
                                Number = counter,
                                Request = request
                            });
                        }
                    };
                    twain.TransferError += (object sender, TransferErrorEventArgs e) =>
                    {
                        var myArgs = new ScanResultEventArgs();
                        myArgs.Error = e.Exception.Message;
                        myArgs.Trace = e.Exception.StackTrace;
                        myArgs.Request = request;
                        if (e.Exception.InnerException != null)
                        {
                            myArgs.Error += " " + e.Exception.InnerException.Message;
                            myArgs.Trace += " " + e.Exception.InnerException.StackTrace;
                        }
                        this.Scanned?.Invoke(null, myArgs);
                    };
                    var rc = twain.Open();
                    if (rc == ReturnCode.Success)
                    {
                        var src = twain.FirstOrDefault(r => r.Name == request.ScannerId);
                        if (src != null)
                        {
                            rc = src.Open();
                            src.Capabilities.ICapYResolution.SetValue(request.Resolution);
                            src.Capabilities.ICapXResolution.SetValue(request.Resolution);
                            src.Capabilities.CapAutoFeed.SetValue(BoolType.True);
                            src.Capabilities.CapAutoScan.SetValue(BoolType.True);
                            src.Capabilities.ICapPixelType.SetValue(PixelType.Gray);
                            src.Capabilities.ICapXferMech.SetValue(XferMech.Native);

                            if (rc == ReturnCode.Success)
                            {
                                rc = src.Enable(SourceEnableMode.NoUI, false, IntPtr.Zero);
                            }
                            else
                            {
                                this.Scanned?.Invoke(null, new ScanResultEventArgs
                                {
                                    Error = "error connecting to scanner",
                                    Request = request
                                });
                            }
                        }
                        else
                        {
                            this.Scanned?.Invoke(null, new ScanResultEventArgs
                            {
                                Error = "error connecting to scanner",
                                Request = request
                            });
                        }
                        
                        twain.Close();
                    }
                }
                Thread.Sleep(500);
            }
        }
    }
}
