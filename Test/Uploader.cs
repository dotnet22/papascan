﻿using MessagingToolkit.Barcode;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Test
{
    class Uploader
    {
        private bool running;
        private readonly object runningLock = new object();
        private ConcurrentQueue<Scanner.ScanResultEventArgs> queue = new ConcurrentQueue<Scanner.ScanResultEventArgs>();
        public Uploader()
        {

        }

        public void Start()
        {
            lock(runningLock)
                this.running = true;
            ThreadPool.QueueUserWorkItem((_) =>
            {
                this.worker();
            });
        }

        public void Stop()
        {
            lock (runningLock)
                this.running = false;
        }

        public void Add(Scanner.ScanResultEventArgs job)
        {
            this.queue.Enqueue(job);
        }

        private void worker()
        {
            while (true)
            {
                lock (runningLock)
                    if (!this.running)
                        break;
                if (this.queue.TryDequeue(out Scanner.ScanResultEventArgs request))
                {
                    string barcode = "";
                    if (!string.IsNullOrEmpty(request.Page))
                    {
                        try
                        {
                            WriteableBitmap bitmap = new WriteableBitmap(BitmapFromUri(new Uri(request.Page, UriKind.Relative)));

                            BarcodeDecoder decoder = new BarcodeDecoder();
                            Dictionary<DecodeOptions, object> dictionary = new Dictionary<DecodeOptions, object>();
                            List<BarcodeFormat> list = new List<BarcodeFormat>
                            {
                                BarcodeFormat.Code128,
                                BarcodeFormat.PDF417,
                                BarcodeFormat.QRCode,
                            };
                            dictionary.Add(DecodeOptions.TryHarder, true);
                            dictionary.Add(DecodeOptions.PossibleFormats, list);
                            var result = decoder.Decode(bitmap, dictionary);
                            barcode = result.Text;
                        }
                        catch (Exception) { }
                    }
                    
                    NameValueCollection nvc = new NameValueCollection
                    {
                        { "barcode", barcode },
                        { "number", request.Number.ToString() },
                        { "error", request.Error },
                        { "trace", request.Trace },
                    };
                    if (!string.IsNullOrEmpty(request.Page))
                    {
                        UploadFile(request.Request.Callback, request.Page, "page", "image/jpeg", nvc);
                        File.Delete(request.Page);
                    }
                    else
                    {
                        UploadForm(request.Request.Callback, nvc);
                    }
                }
                Thread.Sleep(500);
            }
        }

        public static BitmapImage BitmapFromUri(Uri source)
        {
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = source;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }

        private static void UploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
            }
            finally
            {
                wr = null;
            }
        }

        private static void UploadForm(string url, NameValueCollection nvc)
        {
            using(WebClient wc = new WebClient())
            {
                wc.UploadValues(url, "POST", nvc);
                
            }
        }
    }
}
