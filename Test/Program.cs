﻿using Newtonsoft.Json;
using NTwain;
using NTwain.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Scanner.Service scanner = new Scanner.Service();
            scanner.Start();
            Uploader uploader = new Uploader();
            uploader.Start();
            scanner.Scanned += (_, e) =>
            {
                uploader.Add(e);
            };
            HttpServer http = new HttpServer(10002);
            http.HandlerFunc("scan", (request, response) => {
                var scanRequest = Deserialize<Scanner.Request>(request.InputStream);
                if(scanRequest != null)
                {
                    scanner.AddJob(scanRequest);
                }
                response.StatusCode = 200;
            });
            http.HandlerFunc("properties", (request, response) => {
                Serialize(response.OutputStream, scanner.Properties);
            });
            http.Start();
            Console.WriteLine("started");
            Console.Read();
        }

        public static void Serialize(Stream s, object value)
        {
            using (StreamWriter writer = new StreamWriter(s))
            using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
            {
                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(jsonWriter, value);
                jsonWriter.Flush();
            }
        }

        public static T Deserialize<T>(Stream s)
        {
            using (StreamReader reader = new StreamReader(s))
            using (JsonTextReader jsonReader = new JsonTextReader(reader))
            {
                JsonSerializer ser = new JsonSerializer();
                return ser.Deserialize<T>(jsonReader);
            }
        }
    }
}
