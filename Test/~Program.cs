﻿//using NTwain;
//using NTwain.Data;
//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Drawing.Imaging;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Test
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            var appId = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly());

//            // new it up and handle events
//            var twain = new TwainSession(appId);

//            twain.TransferReady += Session_TransferReady;
//            twain.DataTransferred += Session_DataTransferred;
//            twain.TransferError += Twain_TransferError;
            

//            // finally open it
//            var rc = twain.Open();
            
//            if (rc == ReturnCode.Success)
//            {
//                var src = twain.FirstOrDefault();
//                rc = src.Open();
//                Console.WriteLine(src.Capabilities.ICapImageFileFormat.CanSet);
//                Console.WriteLine(src.Capabilities.ICapImageFileFormat.GetValues().FirstOrDefault());
//                //src.Capabilities.ICapImageFileFormat.SetValue(FileFormat.Pdf);
//                src.Capabilities.ICapYResolution.SetValue(300);
//                src.Capabilities.ICapXResolution.SetValue(300);
//                src.Capabilities.CapAutoFeed.SetValue(BoolType.True);
//                src.Capabilities.CapAutoScan.SetValue(BoolType.True);
//                src.Capabilities.ICapPixelType.SetValue(PixelType.Gray);
//                //src.Capabilities.ICapJpegQuality.SetValue(JpegQuality.Medium);
//                src.Capabilities.ICapXferMech.SetValue(XferMech.Native);
//                Console.WriteLine(src.Capabilities.ICapImageFileFormat.GetCurrent());
                
//                if (rc == ReturnCode.Success)
//                {
//                    Console.WriteLine("Starting capture from the sample source...");
//                    rc = src.Enable(SourceEnableMode.NoUI, false, IntPtr.Zero);
//                }
//                else
//                {
//                    Console.WriteLine("error");
//                    twain.Close();
//                }
//            }
//            Console.Read();
//        }

//        private static void Twain_TransferError(object sender, TransferErrorEventArgs e)
//        {
//            Console.WriteLine("Twain_TransferError");
//        }

//        private static void Session_DataTransferred(object sender, DataTransferredEventArgs e)
//        {
//            if (e.NativeData != IntPtr.Zero)
//            {
//                //var stream = e.GetNativeImageStream();
//                //using (Stream file = File.Create(@"C:\Users\Anatoliy\OneDrive\Projects\dotNet\PapaScan\test.jpeg"))
//                //{
//                //    stream.CopyTo(file);
//                //}
//                var img = Image.FromStream(e.GetNativeImageStream());
//                img.Save(@"C:\Users\Anatoliy\OneDrive\Projects\dotNet\PapaScan\test.jpeg", ImageFormat.Jpeg);
//            }
//            else
//            {
//                Console.WriteLine("BUMMER! No twain data on thread {0}.", Thread.CurrentThread.ManagedThreadId);
//            }
//        }

//        private static void Session_TransferReady(object sender, TransferReadyEventArgs e)
//        {
//            Console.WriteLine("Session_TransferReady");
//        }
//    }
//}
