﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Test
{
    class HttpServer
    {
        private HttpListener listener;
        private short port;
        public delegate void Handler(HttpListenerRequest request, HttpListenerResponse response);
        private Dictionary<string, Handler> handlers;

        public HttpServer(short port)
        {
            this.port = port;
            this.handlers = new Dictionary<string, Handler>();
            listener = new HttpListener();
            listener.Prefixes.Add(string.Format("http://127.0.0.1:{0}/", port));
        }

        public void HandlerFunc(string path, Handler handler)
        {
            this.handlers.Add("/" + path, handler);
        }

        public async void Start()
        {
            listener.Start();
            while (true)
            {
                HttpListenerContext context = await listener.GetContextAsync();

                if (this.handlers.ContainsKey(context.Request.Url.LocalPath))
                {
                    this.handlers[context.Request.Url.LocalPath](context.Request, context.Response);
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
                context.Response.Headers.Add("Access-Control-Allow-Origin: *");
                context.Response.OutputStream.Close();
            }
        }

        public void Stop()
        {
            listener.Stop();
        }
    }
}